DevOps Coding Challenge
=======================

We asked our junior developer to write a program to read from two
web services and report on the status of all the feature branches.

He made a good start, but decided to leave before finishing his work.
Please take a look at his code and finish up anything you see that
needs work.

To complete this challenge:

* Create a github account if you do not already have one.  
* Fork this repository into your own account.
* Clone your repository to a machine that has java and maven installed.
* Make changes to the source files
* Commit your changes, push to your repository, and create a pull request.
